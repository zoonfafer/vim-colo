{
  description = "Generate Vim colour scheme files declaratively from YAML";
  inputs = rec {
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils,... }@inputs:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
          packageName = "vim-colo";
          app = pkgs.callPackage ./default.nix {};
        in
        rec {
          defaultPackage = self.packages.${system}.${packageName};
          packages.${packageName} = app;
        }
      );
}
