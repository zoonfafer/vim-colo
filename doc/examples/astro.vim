" Theme: astro
" Author: Anonymous
" License: MIT
" Source: TBD

fun! s:identify_color_mode()
  let s:MODE_16_COLOR = 0
  let s:MODE_256_COLOR = 1
  let s:MODE_GUI_COLOR = 2

  if has("gui_running")  || has('termguicolors') && &termguicolors || has('nvim') && $NVIM_TUI_ENABLE_TRUE_COLOR
    let s:mode = s:MODE_GUI_COLOR
  elseif (&t_Co >= 256)
    let s:mode = s:MODE_256_COLOR
  else
    let s:mode = s:MODE_16_COLOR
  endif
endfun

" returns the palette index to approximate the 'rrggbb' hex string
fun s:rgb(rgb)
  let l:r = ("0x" . strpart(a:rgb, 0, 2)) + 0
  let l:g = ("0x" . strpart(a:rgb, 2, 2)) + 0
  let l:b = ("0x" . strpart(a:rgb, 4, 2)) + 0

  return s:color(l:r, l:g, l:b)
endfun

" sets the highlighting for the given group
fun s:X(group, fg, bg, attr)
  if a:fg =~ "[^a-fA-F0-9]"
    exec "hi " . a:group . " guifg=" . a:fg . " ctermfg=" .  a:fg
  elseif a:fg != ""
    exec "hi " . a:group . " guifg=#" . a:fg . " ctermfg=" . s:rgb(a:fg)
  endif

  if a:bg =~ "[^a-fA-F0-9]"
    exec "hi " . a:group . " guibg=" . a:bg . " ctermbg=" . a:bg
  elseif a:bg != ""
    exec "hi " . a:group . " guibg=#" . a:bg . " ctermbg=" . s:rgb(a:bg)
  endif

  if a:attr != ""
    exec "hi " . a:group . " gui=" . a:attr . " cterm=" . a:attr
  endif
endfun

fun! s:apply_styles()
  hi clear
  syntax reset
  let g:colors_name = 'astro'

  let s:bg_is_dark = &background ==# 'dark'
  if s:bg_is_dark

    if s:mode == s:MODE_GUI_COLOR

      hi Comment        guifg=#a00000 guibg=#100000 cterm=NONE gui=NONE
      hi Constant       guifg=NONE guibg=NONE cterm=bold gui=bold
      hi String         guifg=#b00000 guibg=NONE cterm=NONE gui=NONE
      hi Character      guifg=#900000 guibg=#100000 cterm=NONE gui=NONE
      hi Number         guifg=#ab0000 guibg=NONE cterm=NONE gui=NONE
      hi Boolean        guifg=#990000 guibg=NONE cterm=bold gui=bold
      hi Float          guifg=#ab0000 guibg=#0a0000 cterm=NONE gui=NONE
      hi Identifier     guifg=NONE guibg=#300000 cterm=NONE gui=NONE
      hi Function       guifg=#a00000 guibg=NONE cterm=NONE gui=NONE
      hi Statement      guifg=#b00000 guibg=#100000 cterm=NONE gui=NONE
      hi Conditional    guifg=#9f0000 guibg=NONE cterm=bold gui=bold
      hi Repeat         guifg=#af0000 guibg=NONE cterm=bold gui=bold
      hi Label          guifg=#b00000 guibg=#200000 cterm=bold gui=bold
      hi Operator       guifg=NONE guibg=#200000 cterm=bold gui=bold
      hi Keyword        guifg=NONE guibg=NONE cterm=italic gui=italic
      hi Exception      guifg=NONE guibg=NONE cterm=bold gui=bold
      hi PreProc        guifg=NONE guibg=#300000 cterm=italic gui=italic
      hi Include        guifg=#a00000 guibg=NONE cterm=bold gui=bold
      hi Define         guifg=#a00000 guibg=#200000 cterm=bold gui=bold
      hi Macro          guifg=#d00000 guibg=NONE cterm=italic gui=italic
      hi PreCondit      guifg=#df0000 guibg=NONE cterm=italic gui=italic
      hi Type           guifg=#b80000 guibg=NONE cterm=italic gui=italic
      hi StorageClass   guifg=#c00000 guibg=#100000 cterm=NONE gui=NONE
      hi Structure      guifg=#d00000 guibg=#100000 cterm=NONE gui=NONE
      hi Typedef        guifg=#b80000 guibg=NONE cterm=italic gui=italic
      hi Special        guifg=#900000 guibg=NONE cterm=bold gui=bold
      hi SpecialChar    guifg=#9f0000 guibg=NONE cterm=NONE gui=NONE
      hi Tag            guifg=#e00000 guibg=NONE cterm=underline gui=underline
      hi Delimiter      guifg=#f00000 guibg=NONE cterm=bold gui=bold
      hi SpecialComment guifg=NONE guibg=#200000 cterm=bold,italic gui=bold,italic
      hi Debug          guifg=#800000 guibg=NONE cterm=NONE gui=NONE
      hi Underlined     guifg=NONE guibg=#200000 cterm=bold,italic,underline gui=bold,italic,underline
      hi Ignore         guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Error          guifg=#000000 guibg=#ff0000 cterm=bold gui=bold
      hi Todo           guifg=#000000 guibg=#900000 cterm=bold gui=bold
      hi keywords       guifg=#ff1111 guibg=NONE cterm=underline gui=underline
      hi ColorColumn    guifg=NONE guibg=#280000 cterm=NONE gui=NONE
      hi Conceal        guifg=#d80000 guibg=NONE cterm=NONE gui=NONE
      hi Cursor         guifg=NONE guibg=#280000 cterm=NONE gui=NONE
      hi CursorIM       guifg=NONE guibg=#380000 cterm=NONE gui=NONE
      hi CursorColumn   guifg=NONE guibg=#380000 cterm=NONE gui=NONE
      hi CursorLine     guifg=NONE guibg=#380000 cterm=NONE gui=NONE
      hi Directory      guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi DiffAdd        guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi DiffChange     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi DiffDelete     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi DiffText       guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi EndOfBuffer    guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi TermCursor     guifg=#f00000 guibg=NONE cterm=NONE gui=NONE
      hi TermCursorNC   guifg=#a00000 guibg=NONE cterm=NONE gui=NONE
      hi ErrorMsg       guifg=NONE guibg=#200000 cterm=NONE gui=NONE
      hi VertSplit      guifg=#d00000 guibg=NONE cterm=NONE gui=NONE
      hi Folded         guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi FoldColumn     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi SignColumn     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi IncSearch      guifg=NONE guibg=#300000 cterm=NONE gui=NONE
      hi Substitute     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi LineNr         guifg=#b00000 guibg=NONE cterm=NONE gui=NONE
      hi CursorLineNr   guifg=#d00000 guibg=NONE cterm=NONE gui=NONE
      hi MatchParen     guifg=#ff0000 guibg=#600000 cterm=bold gui=bold
      hi ModeMsg        guifg=#b00000 guibg=NONE cterm=bold gui=bold
      hi MsgArea        guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi MsgSeparator   guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi MoreMsg        guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi NonText        guifg=#500000 guibg=NONE cterm=NONE gui=NONE
      hi Normal         guifg=#e00000 guibg=#000000 cterm=NONE gui=NONE
      hi NormalFloat    guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi NormalNC       guifg=#800000 guibg=#100000 cterm=NONE gui=NONE
      hi Pmenu          guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi PmenuSel       guifg=NONE guibg=#500000 cterm=NONE gui=NONE
      hi PmenuSbar      guifg=#e00000 guibg=#200000 cterm=NONE gui=NONE
      hi PmenuThumb     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Question       guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi QuickFixLine   guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Search         guifg=#000000 guibg=#b00000 cterm=bold gui=bold
      hi SpecialKey     guifg=#800000 guibg=NONE cterm=NONE gui=NONE
      hi SpellBad       guifg=NONE guibg=#800000 cterm=underline gui=underline
      hi SpellCap       guifg=NONE guibg=NONE cterm=bold gui=bold
      hi SpellLocal     guifg=NONE guibg=NONE cterm=bold gui=bold
      hi SpellRare      guifg=NONE guibg=NONE cterm=italic gui=italic
      hi StatusLine     guifg=#a00000 guibg=NONE cterm=underline gui=underline
      hi StatusLineNC   guifg=#a00000 guibg=#400000 cterm=underline gui=underline
      hi TabLine        guifg=#a00000 guibg=#400000 cterm=NONE gui=NONE
      hi TabLineFill    guifg=NONE guibg=#400000 cterm=NONE gui=NONE
      hi TabLineSel     guifg=NONE guibg=NONE cterm=bold gui=bold
      hi Title          guifg=#c00000 guibg=NONE cterm=bold gui=bold
      hi Visual         guifg=NONE guibg=#500000 cterm=bold gui=bold
      hi VisualNOS      guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi WarningMsg     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Whitespace     guifg=#600000 guibg=NONE cterm=NONE gui=NONE
      hi WildMenu       guifg=NONE guibg=#400000 cterm=NONE gui=NONE
      hi Menu           guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Scrollbar      guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Tooltip        guifg=NONE guibg=NONE cterm=NONE gui=NONE

    elseif s:mode == s:MODE_256_COLOR

      hi Comment        ctermfg=#a00000 ctermbg=#100000 cterm=NONE
      hi Constant       ctermfg=NONE ctermbg=NONE cterm=bold
      hi String         ctermfg=#b00000 ctermbg=NONE cterm=NONE
      hi Character      ctermfg=#900000 ctermbg=#100000 cterm=NONE
      hi Number         ctermfg=#ab0000 ctermbg=NONE cterm=NONE
      hi Boolean        ctermfg=#990000 ctermbg=NONE cterm=bold
      hi Float          ctermfg=#ab0000 ctermbg=#0a0000 cterm=NONE
      hi Identifier     ctermfg=NONE ctermbg=#300000 cterm=NONE
      hi Function       ctermfg=#a00000 ctermbg=NONE cterm=NONE
      hi Statement      ctermfg=#b00000 ctermbg=#100000 cterm=NONE
      hi Conditional    ctermfg=#9f0000 ctermbg=NONE cterm=bold
      hi Repeat         ctermfg=#af0000 ctermbg=NONE cterm=bold
      hi Label          ctermfg=#b00000 ctermbg=#200000 cterm=bold
      hi Operator       ctermfg=NONE ctermbg=#200000 cterm=bold
      hi Keyword        ctermfg=NONE ctermbg=NONE cterm=italic
      hi Exception      ctermfg=NONE ctermbg=NONE cterm=bold
      hi PreProc        ctermfg=NONE ctermbg=#300000 cterm=italic
      hi Include        ctermfg=#a00000 ctermbg=NONE cterm=bold
      hi Define         ctermfg=#a00000 ctermbg=#200000 cterm=bold
      hi Macro          ctermfg=#d00000 ctermbg=NONE cterm=italic
      hi PreCondit      ctermfg=#df0000 ctermbg=NONE cterm=italic
      hi Type           ctermfg=#b80000 ctermbg=NONE cterm=italic
      hi StorageClass   ctermfg=#c00000 ctermbg=#100000 cterm=NONE
      hi Structure      ctermfg=#d00000 ctermbg=#100000 cterm=NONE
      hi Typedef        ctermfg=#b80000 ctermbg=NONE cterm=italic
      hi Special        ctermfg=#900000 ctermbg=NONE cterm=bold
      hi SpecialChar    ctermfg=#9f0000 ctermbg=NONE cterm=NONE
      hi Tag            ctermfg=#e00000 ctermbg=NONE cterm=underline
      hi Delimiter      ctermfg=#f00000 ctermbg=NONE cterm=bold
      hi SpecialComment ctermfg=NONE ctermbg=#200000 cterm=bold,italic
      hi Debug          ctermfg=#800000 ctermbg=NONE cterm=NONE
      hi Underlined     ctermfg=NONE ctermbg=#200000 cterm=bold,italic,underline
      hi Ignore         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Error          ctermfg=#000000 ctermbg=#ff0000 cterm=bold
      hi Todo           ctermfg=#000000 ctermbg=#900000 cterm=bold
      hi keywords       ctermfg=#ff1111 ctermbg=NONE cterm=underline
      hi ColorColumn    ctermfg=NONE ctermbg=#280000 cterm=NONE
      hi Conceal        ctermfg=#d80000 ctermbg=NONE cterm=NONE
      hi Cursor         ctermfg=NONE ctermbg=#280000 cterm=NONE
      hi CursorIM       ctermfg=NONE ctermbg=#380000 cterm=NONE
      hi CursorColumn   ctermfg=NONE ctermbg=#380000 cterm=NONE
      hi CursorLine     ctermfg=NONE ctermbg=#380000 cterm=NONE
      hi Directory      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffAdd        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffChange     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffDelete     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffText       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi EndOfBuffer    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi TermCursor     ctermfg=#f00000 ctermbg=NONE cterm=NONE
      hi TermCursorNC   ctermfg=#a00000 ctermbg=NONE cterm=NONE
      hi ErrorMsg       ctermfg=NONE ctermbg=#200000 cterm=NONE
      hi VertSplit      ctermfg=#d00000 ctermbg=NONE cterm=NONE
      hi Folded         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi FoldColumn     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi SignColumn     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi IncSearch      ctermfg=NONE ctermbg=#300000 cterm=NONE
      hi Substitute     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi LineNr         ctermfg=#b00000 ctermbg=NONE cterm=NONE
      hi CursorLineNr   ctermfg=#d00000 ctermbg=NONE cterm=NONE
      hi MatchParen     ctermfg=#ff0000 ctermbg=#600000 cterm=bold
      hi ModeMsg        ctermfg=#b00000 ctermbg=NONE cterm=bold
      hi MsgArea        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi MsgSeparator   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi MoreMsg        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi NonText        ctermfg=#500000 ctermbg=NONE cterm=NONE
      hi Normal         ctermfg=#e00000 ctermbg=#000000 cterm=NONE
      hi NormalFloat    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi NormalNC       ctermfg=#800000 ctermbg=#100000 cterm=NONE
      hi Pmenu          ctermfg=NONE ctermbg=NONE cterm=NONE
      hi PmenuSel       ctermfg=NONE ctermbg=#500000 cterm=NONE
      hi PmenuSbar      ctermfg=#e00000 ctermbg=#200000 cterm=NONE
      hi PmenuThumb     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Question       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi QuickFixLine   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Search         ctermfg=#000000 ctermbg=#b00000 cterm=bold
      hi SpecialKey     ctermfg=#800000 ctermbg=NONE cterm=NONE
      hi SpellBad       ctermfg=NONE ctermbg=#800000 cterm=underline
      hi SpellCap       ctermfg=NONE ctermbg=NONE cterm=bold
      hi SpellLocal     ctermfg=NONE ctermbg=NONE cterm=bold
      hi SpellRare      ctermfg=NONE ctermbg=NONE cterm=italic
      hi StatusLine     ctermfg=#a00000 ctermbg=NONE cterm=underline
      hi StatusLineNC   ctermfg=#a00000 ctermbg=#400000 cterm=underline
      hi TabLine        ctermfg=#a00000 ctermbg=#400000 cterm=NONE
      hi TabLineFill    ctermfg=NONE ctermbg=#400000 cterm=NONE
      hi TabLineSel     ctermfg=NONE ctermbg=NONE cterm=bold
      hi Title          ctermfg=#c00000 ctermbg=NONE cterm=bold
      hi Visual         ctermfg=NONE ctermbg=#500000 cterm=bold
      hi VisualNOS      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi WarningMsg     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Whitespace     ctermfg=#600000 ctermbg=NONE cterm=NONE
      hi WildMenu       ctermfg=NONE ctermbg=#400000 cterm=NONE
      hi Menu           ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Scrollbar      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Tooltip        ctermfg=NONE ctermbg=NONE cterm=NONE

    else " Terminal colours

      hi Comment        ctermfg=#a00000 ctermbg=#100000 cterm=NONE
      hi Constant       ctermfg=NONE ctermbg=NONE cterm=
      hi String         ctermfg=#b00000 ctermbg=NONE cterm=NONE
      hi Character      ctermfg=#900000 ctermbg=#100000 cterm=NONE
      hi Number         ctermfg=#ab0000 ctermbg=NONE cterm=NONE
      hi Boolean        ctermfg=#990000 ctermbg=NONE cterm=
      hi Float          ctermfg=#ab0000 ctermbg=#0a0000 cterm=NONE
      hi Identifier     ctermfg=NONE ctermbg=#300000 cterm=NONE
      hi Function       ctermfg=#a00000 ctermbg=NONE cterm=NONE
      hi Statement      ctermfg=#b00000 ctermbg=#100000 cterm=NONE
      hi Conditional    ctermfg=#9f0000 ctermbg=NONE cterm=
      hi Repeat         ctermfg=#af0000 ctermbg=NONE cterm=
      hi Label          ctermfg=#b00000 ctermbg=#200000 cterm=
      hi Operator       ctermfg=NONE ctermbg=#200000 cterm=
      hi Keyword        ctermfg=NONE ctermbg=NONE cterm=
      hi Exception      ctermfg=NONE ctermbg=NONE cterm=
      hi PreProc        ctermfg=NONE ctermbg=#300000 cterm=
      hi Include        ctermfg=#a00000 ctermbg=NONE cterm=
      hi Define         ctermfg=#a00000 ctermbg=#200000 cterm=
      hi Macro          ctermfg=#d00000 ctermbg=NONE cterm=
      hi PreCondit      ctermfg=#df0000 ctermbg=NONE cterm=
      hi Type           ctermfg=#b80000 ctermbg=NONE cterm=
      hi StorageClass   ctermfg=#c00000 ctermbg=#100000 cterm=NONE
      hi Structure      ctermfg=#d00000 ctermbg=#100000 cterm=NONE
      hi Typedef        ctermfg=#b80000 ctermbg=NONE cterm=
      hi Special        ctermfg=#900000 ctermbg=NONE cterm=
      hi SpecialChar    ctermfg=#9f0000 ctermbg=NONE cterm=NONE
      hi Tag            ctermfg=#e00000 ctermbg=NONE cterm=
      hi Delimiter      ctermfg=#f00000 ctermbg=NONE cterm=
      hi SpecialComment ctermfg=NONE ctermbg=#200000 cterm=,
      hi Debug          ctermfg=#800000 ctermbg=NONE cterm=NONE
      hi Underlined     ctermfg=NONE ctermbg=#200000 cterm=,,
      hi Ignore         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Error          ctermfg=#000000 ctermbg=#ff0000 cterm=
      hi Todo           ctermfg=#000000 ctermbg=#900000 cterm=
      hi keywords       ctermfg=#ff1111 ctermbg=NONE cterm=
      hi ColorColumn    ctermfg=NONE ctermbg=#280000 cterm=NONE
      hi Conceal        ctermfg=#d80000 ctermbg=NONE cterm=NONE
      hi Cursor         ctermfg=NONE ctermbg=#280000 cterm=NONE
      hi CursorIM       ctermfg=NONE ctermbg=#380000 cterm=NONE
      hi CursorColumn   ctermfg=NONE ctermbg=#380000 cterm=NONE
      hi CursorLine     ctermfg=NONE ctermbg=#380000 cterm=NONE
      hi Directory      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffAdd        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffChange     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffDelete     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffText       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi EndOfBuffer    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi TermCursor     ctermfg=#f00000 ctermbg=NONE cterm=NONE
      hi TermCursorNC   ctermfg=#a00000 ctermbg=NONE cterm=NONE
      hi ErrorMsg       ctermfg=NONE ctermbg=#200000 cterm=NONE
      hi VertSplit      ctermfg=#d00000 ctermbg=NONE cterm=NONE
      hi Folded         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi FoldColumn     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi SignColumn     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi IncSearch      ctermfg=NONE ctermbg=#300000 cterm=NONE
      hi Substitute     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi LineNr         ctermfg=#b00000 ctermbg=NONE cterm=NONE
      hi CursorLineNr   ctermfg=#d00000 ctermbg=NONE cterm=NONE
      hi MatchParen     ctermfg=#ff0000 ctermbg=#600000 cterm=
      hi ModeMsg        ctermfg=#b00000 ctermbg=NONE cterm=
      hi MsgArea        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi MsgSeparator   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi MoreMsg        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi NonText        ctermfg=#500000 ctermbg=NONE cterm=NONE
      hi Normal         ctermfg=#e00000 ctermbg=#000000 cterm=NONE
      hi NormalFloat    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi NormalNC       ctermfg=#800000 ctermbg=#100000 cterm=NONE
      hi Pmenu          ctermfg=NONE ctermbg=NONE cterm=NONE
      hi PmenuSel       ctermfg=NONE ctermbg=#500000 cterm=NONE
      hi PmenuSbar      ctermfg=#e00000 ctermbg=#200000 cterm=NONE
      hi PmenuThumb     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Question       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi QuickFixLine   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Search         ctermfg=#000000 ctermbg=#b00000 cterm=
      hi SpecialKey     ctermfg=#800000 ctermbg=NONE cterm=NONE
      hi SpellBad       ctermfg=NONE ctermbg=#800000 cterm=
      hi SpellCap       ctermfg=NONE ctermbg=NONE cterm=
      hi SpellLocal     ctermfg=NONE ctermbg=NONE cterm=
      hi SpellRare      ctermfg=NONE ctermbg=NONE cterm=
      hi StatusLine     ctermfg=#a00000 ctermbg=NONE cterm=
      hi StatusLineNC   ctermfg=#a00000 ctermbg=#400000 cterm=
      hi TabLine        ctermfg=#a00000 ctermbg=#400000 cterm=NONE
      hi TabLineFill    ctermfg=NONE ctermbg=#400000 cterm=NONE
      hi TabLineSel     ctermfg=NONE ctermbg=NONE cterm=
      hi Title          ctermfg=#c00000 ctermbg=NONE cterm=
      hi Visual         ctermfg=NONE ctermbg=#500000 cterm=
      hi VisualNOS      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi WarningMsg     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Whitespace     ctermfg=#600000 ctermbg=NONE cterm=NONE
      hi WildMenu       ctermfg=NONE ctermbg=#400000 cterm=NONE
      hi Menu           ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Scrollbar      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Tooltip        ctermfg=NONE ctermbg=NONE cterm=NONE
    endif

  else

    if s:mode == s:MODE_GUI_COLOR

      hi Comment        guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Constant       guifg=NONE guibg=NONE cterm=bold gui=bold
      hi String         guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Character      guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Number         guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Boolean        guifg=NONE guibg=NONE cterm=bold gui=bold
      hi Float          guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Identifier     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Function       guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Statement      guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Conditional    guifg=NONE guibg=NONE cterm=bold gui=bold
      hi Repeat         guifg=NONE guibg=NONE cterm=bold gui=bold
      hi Label          guifg=NONE guibg=NONE cterm=bold gui=bold
      hi Operator       guifg=NONE guibg=NONE cterm=bold gui=bold
      hi Keyword        guifg=NONE guibg=NONE cterm=italic gui=italic
      hi Exception      guifg=NONE guibg=NONE cterm=bold gui=bold
      hi PreProc        guifg=NONE guibg=NONE cterm=italic gui=italic
      hi Include        guifg=NONE guibg=NONE cterm=bold gui=bold
      hi Define         guifg=NONE guibg=NONE cterm=bold gui=bold
      hi Macro          guifg=NONE guibg=NONE cterm=italic gui=italic
      hi PreCondit      guifg=NONE guibg=NONE cterm=italic gui=italic
      hi Type           guifg=NONE guibg=NONE cterm=italic gui=italic
      hi StorageClass   guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Structure      guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Typedef        guifg=NONE guibg=NONE cterm=italic gui=italic
      hi Special        guifg=NONE guibg=NONE cterm=bold gui=bold
      hi SpecialChar    guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Tag            guifg=NONE guibg=NONE cterm=underline gui=underline
      hi Delimiter      guifg=NONE guibg=NONE cterm=bold gui=bold
      hi SpecialComment guifg=NONE guibg=NONE cterm=bold,italic gui=bold,italic
      hi Debug          guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Underlined     guifg=NONE guibg=NONE cterm=bold,italic,underline gui=bold,italic,underline
      hi Ignore         guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Error          guifg=NONE guibg=NONE cterm=bold gui=bold
      hi Todo           guifg=NONE guibg=NONE cterm=bold gui=bold
      hi keywords       guifg=NONE guibg=NONE cterm=underline gui=underline
      hi ColorColumn    guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Conceal        guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Cursor         guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi CursorIM       guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi CursorColumn   guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi CursorLine     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Directory      guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi DiffAdd        guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi DiffChange     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi DiffDelete     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi DiffText       guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi EndOfBuffer    guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi TermCursor     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi TermCursorNC   guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi ErrorMsg       guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi VertSplit      guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Folded         guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi FoldColumn     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi SignColumn     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi IncSearch      guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Substitute     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi LineNr         guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi CursorLineNr   guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi MatchParen     guifg=NONE guibg=NONE cterm=bold gui=bold
      hi ModeMsg        guifg=NONE guibg=there:ansparent cterm=bold gui=bold
      hi MsgArea        guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi MsgSeparator   guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi MoreMsg        guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi NonText        guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Normal         guifg=#000000 guibg=#b00000 cterm=NONE gui=NONE
      hi NormalFloat    guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi NormalNC       guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Pmenu          guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi PmenuSel       guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi PmenuSbar      guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi PmenuThumb     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Question       guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi QuickFixLine   guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Search         guifg=NONE guibg=NONE cterm=bold gui=bold
      hi SpecialKey     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi SpellBad       guifg=NONE guibg=NONE cterm=underline gui=underline
      hi SpellCap       guifg=NONE guibg=NONE cterm=bold gui=bold
      hi SpellLocal     guifg=NONE guibg=NONE cterm=bold gui=bold
      hi SpellRare      guifg=NONE guibg=NONE cterm=italic gui=italic
      hi StatusLine     guifg=NONE guibg=NONE cterm=underline gui=underline
      hi StatusLineNC   guifg=NONE guibg=NONE cterm=underline gui=underline
      hi TabLine        guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi TabLineFill    guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi TabLineSel     guifg=NONE guibg=NONE cterm=bold gui=bold
      hi Title          guifg=NONE guibg=NONE cterm=bold gui=bold
      hi Visual         guifg=NONE guibg=NONE cterm=bold gui=bold
      hi VisualNOS      guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi WarningMsg     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Whitespace     guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi WildMenu       guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Menu           guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Scrollbar      guifg=NONE guibg=NONE cterm=NONE gui=NONE
      hi Tooltip        guifg=NONE guibg=NONE cterm=NONE gui=NONE

    elseif s:mode == s:MODE_256_COLOR

      hi Comment        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Constant       ctermfg=NONE ctermbg=NONE cterm=bold
      hi String         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Character      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Number         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Boolean        ctermfg=NONE ctermbg=NONE cterm=bold
      hi Float          ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Identifier     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Function       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Statement      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Conditional    ctermfg=NONE ctermbg=NONE cterm=bold
      hi Repeat         ctermfg=NONE ctermbg=NONE cterm=bold
      hi Label          ctermfg=NONE ctermbg=NONE cterm=bold
      hi Operator       ctermfg=NONE ctermbg=NONE cterm=bold
      hi Keyword        ctermfg=NONE ctermbg=NONE cterm=italic
      hi Exception      ctermfg=NONE ctermbg=NONE cterm=bold
      hi PreProc        ctermfg=NONE ctermbg=NONE cterm=italic
      hi Include        ctermfg=NONE ctermbg=NONE cterm=bold
      hi Define         ctermfg=NONE ctermbg=NONE cterm=bold
      hi Macro          ctermfg=NONE ctermbg=NONE cterm=italic
      hi PreCondit      ctermfg=NONE ctermbg=NONE cterm=italic
      hi Type           ctermfg=NONE ctermbg=NONE cterm=italic
      hi StorageClass   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Structure      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Typedef        ctermfg=NONE ctermbg=NONE cterm=italic
      hi Special        ctermfg=NONE ctermbg=NONE cterm=bold
      hi SpecialChar    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Tag            ctermfg=NONE ctermbg=NONE cterm=underline
      hi Delimiter      ctermfg=NONE ctermbg=NONE cterm=bold
      hi SpecialComment ctermfg=NONE ctermbg=NONE cterm=bold,italic
      hi Debug          ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Underlined     ctermfg=NONE ctermbg=NONE cterm=bold,italic,underline
      hi Ignore         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Error          ctermfg=NONE ctermbg=NONE cterm=bold
      hi Todo           ctermfg=NONE ctermbg=NONE cterm=bold
      hi keywords       ctermfg=NONE ctermbg=NONE cterm=underline
      hi ColorColumn    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Conceal        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Cursor         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi CursorIM       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi CursorColumn   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi CursorLine     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Directory      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffAdd        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffChange     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffDelete     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffText       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi EndOfBuffer    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi TermCursor     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi TermCursorNC   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi ErrorMsg       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi VertSplit      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Folded         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi FoldColumn     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi SignColumn     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi IncSearch      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Substitute     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi LineNr         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi CursorLineNr   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi MatchParen     ctermfg=NONE ctermbg=NONE cterm=bold
      hi ModeMsg        ctermfg=NONE ctermbg=there:ansparent cterm=bold
      hi MsgArea        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi MsgSeparator   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi MoreMsg        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi NonText        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Normal         ctermfg=#000000 ctermbg=#b00000 cterm=NONE
      hi NormalFloat    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi NormalNC       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Pmenu          ctermfg=NONE ctermbg=NONE cterm=NONE
      hi PmenuSel       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi PmenuSbar      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi PmenuThumb     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Question       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi QuickFixLine   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Search         ctermfg=NONE ctermbg=NONE cterm=bold
      hi SpecialKey     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi SpellBad       ctermfg=NONE ctermbg=NONE cterm=underline
      hi SpellCap       ctermfg=NONE ctermbg=NONE cterm=bold
      hi SpellLocal     ctermfg=NONE ctermbg=NONE cterm=bold
      hi SpellRare      ctermfg=NONE ctermbg=NONE cterm=italic
      hi StatusLine     ctermfg=NONE ctermbg=NONE cterm=underline
      hi StatusLineNC   ctermfg=NONE ctermbg=NONE cterm=underline
      hi TabLine        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi TabLineFill    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi TabLineSel     ctermfg=NONE ctermbg=NONE cterm=bold
      hi Title          ctermfg=NONE ctermbg=NONE cterm=bold
      hi Visual         ctermfg=NONE ctermbg=NONE cterm=bold
      hi VisualNOS      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi WarningMsg     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Whitespace     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi WildMenu       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Menu           ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Scrollbar      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Tooltip        ctermfg=NONE ctermbg=NONE cterm=NONE

    else " Terminal colours

      hi Comment        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Constant       ctermfg=NONE ctermbg=NONE cterm=
      hi String         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Character      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Number         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Boolean        ctermfg=NONE ctermbg=NONE cterm=
      hi Float          ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Identifier     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Function       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Statement      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Conditional    ctermfg=NONE ctermbg=NONE cterm=
      hi Repeat         ctermfg=NONE ctermbg=NONE cterm=
      hi Label          ctermfg=NONE ctermbg=NONE cterm=
      hi Operator       ctermfg=NONE ctermbg=NONE cterm=
      hi Keyword        ctermfg=NONE ctermbg=NONE cterm=
      hi Exception      ctermfg=NONE ctermbg=NONE cterm=
      hi PreProc        ctermfg=NONE ctermbg=NONE cterm=
      hi Include        ctermfg=NONE ctermbg=NONE cterm=
      hi Define         ctermfg=NONE ctermbg=NONE cterm=
      hi Macro          ctermfg=NONE ctermbg=NONE cterm=
      hi PreCondit      ctermfg=NONE ctermbg=NONE cterm=
      hi Type           ctermfg=NONE ctermbg=NONE cterm=
      hi StorageClass   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Structure      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Typedef        ctermfg=NONE ctermbg=NONE cterm=
      hi Special        ctermfg=NONE ctermbg=NONE cterm=
      hi SpecialChar    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Tag            ctermfg=NONE ctermbg=NONE cterm=
      hi Delimiter      ctermfg=NONE ctermbg=NONE cterm=
      hi SpecialComment ctermfg=NONE ctermbg=NONE cterm=,
      hi Debug          ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Underlined     ctermfg=NONE ctermbg=NONE cterm=,,
      hi Ignore         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Error          ctermfg=NONE ctermbg=NONE cterm=
      hi Todo           ctermfg=NONE ctermbg=NONE cterm=
      hi keywords       ctermfg=NONE ctermbg=NONE cterm=
      hi ColorColumn    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Conceal        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Cursor         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi CursorIM       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi CursorColumn   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi CursorLine     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Directory      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffAdd        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffChange     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffDelete     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi DiffText       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi EndOfBuffer    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi TermCursor     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi TermCursorNC   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi ErrorMsg       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi VertSplit      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Folded         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi FoldColumn     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi SignColumn     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi IncSearch      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Substitute     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi LineNr         ctermfg=NONE ctermbg=NONE cterm=NONE
      hi CursorLineNr   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi MatchParen     ctermfg=NONE ctermbg=NONE cterm=
      hi ModeMsg        ctermfg=NONE ctermbg=there:ansparent cterm=
      hi MsgArea        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi MsgSeparator   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi MoreMsg        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi NonText        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Normal         ctermfg=#000000 ctermbg=#b00000 cterm=NONE
      hi NormalFloat    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi NormalNC       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Pmenu          ctermfg=NONE ctermbg=NONE cterm=NONE
      hi PmenuSel       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi PmenuSbar      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi PmenuThumb     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Question       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi QuickFixLine   ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Search         ctermfg=NONE ctermbg=NONE cterm=
      hi SpecialKey     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi SpellBad       ctermfg=NONE ctermbg=NONE cterm=
      hi SpellCap       ctermfg=NONE ctermbg=NONE cterm=
      hi SpellLocal     ctermfg=NONE ctermbg=NONE cterm=
      hi SpellRare      ctermfg=NONE ctermbg=NONE cterm=
      hi StatusLine     ctermfg=NONE ctermbg=NONE cterm=
      hi StatusLineNC   ctermfg=NONE ctermbg=NONE cterm=
      hi TabLine        ctermfg=NONE ctermbg=NONE cterm=NONE
      hi TabLineFill    ctermfg=NONE ctermbg=NONE cterm=NONE
      hi TabLineSel     ctermfg=NONE ctermbg=NONE cterm=
      hi Title          ctermfg=NONE ctermbg=NONE cterm=
      hi Visual         ctermfg=NONE ctermbg=NONE cterm=
      hi VisualNOS      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi WarningMsg     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Whitespace     ctermfg=NONE ctermbg=NONE cterm=NONE
      hi WildMenu       ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Menu           ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Scrollbar      ctermfg=NONE ctermbg=NONE cterm=NONE
      hi Tooltip        ctermfg=NONE ctermbg=NONE cterm=NONE
    endif

  endif
endfun

" ===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-==

call s:identify_color_mode()
call s:apply_styles()

" vim:set et sw=2 fdl=0 fdm=marker:
