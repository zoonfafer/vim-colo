#!/usr/bin/env ruby
# frozen_string_literal: true

#
# Jeffrey Lau <who.else.at.jlau.tk>
# Mon Aug 31 23:16:51 HKT 2020

require 'pp'
require 'yaml'

PROGNAME = $PROGRAM_NAME

def show_usage
  puts "\e[1musage\e[m: #{PROGNAME} [options] YAML_FILENAME DESTINATION_FILENAME"
end

def read_yaml(filename)
  YAML.safe_load(File.read(filename))
end

def theme_meta(_src_file, dest_file)
  {
    'theme_name': dest_file.gsub(%r{^.*/}, '').gsub(/\..*?$/, ''),
    'author':     (ENV['AUTHOR'] || 'Anonymous'),
    'license':    (ENV['LICENCE'] || ENV['LICENSE'] || 'MIT'),
    'source':     (ENV['SOURCE'] || 'TBD'),
  }
end

def do_it(src_file, dest_file)
  yaml = read_yaml(src_file)

  colour_definitions = yaml.values.reduce { |acc, i| acc.merge(i) }

  meta = theme_meta(src_file, dest_file)

  colour_definitions_vim = to_vim(meta[:theme_name], colour_definitions)

  File.open(dest_file, 'w') do |f|
    f.puts prelude(meta)
    f.puts vim_helpers
    f.puts colour_definitions_vim
    f.puts postlude
  end

  # puts File.read(dest_file)
end

def prelude(opts)
  <<~EOSTRING
    " Theme: #{opts[:theme_name]}
    " Author: #{opts[:author]}
    " License: #{opts[:license]}
    " Source: #{opts[:source]}

  EOSTRING
end

def vim_helpers
  <<~EOSTRING
    fun! s:identify_color_mode()
      let s:MODE_16_COLOR = 0
      let s:MODE_256_COLOR = 1
      let s:MODE_GUI_COLOR = 2

      if has("gui_running")  || has('termguicolors') && &termguicolors || has('nvim') && $NVIM_TUI_ENABLE_TRUE_COLOR
        let s:mode = s:MODE_GUI_COLOR
      elseif (&t_Co >= 256)
        let s:mode = s:MODE_256_COLOR
      else
        let s:mode = s:MODE_16_COLOR
      endif
    endfun

    " returns the palette index to approximate the 'rrggbb' hex string
    fun s:rgb(rgb)
      let l:r = ("0x" . strpart(a:rgb, 0, 2)) + 0
      let l:g = ("0x" . strpart(a:rgb, 2, 2)) + 0
      let l:b = ("0x" . strpart(a:rgb, 4, 2)) + 0

      return s:color(l:r, l:g, l:b)
    endfun

    " sets the highlighting for the given group
    fun s:X(group, fg, bg, attr)
      if a:fg =~ "[^a-fA-F0-9]"
        exec "hi " . a:group . " guifg=" . a:fg . " ctermfg=" .  a:fg
      elseif a:fg != ""
        exec "hi " . a:group . " guifg=#" . a:fg . " ctermfg=" . s:rgb(a:fg)
      endif

      if a:bg =~ "[^a-fA-F0-9]"
        exec "hi " . a:group . " guibg=" . a:bg . " ctermbg=" . a:bg
      elseif a:bg != ""
        exec "hi " . a:group . " guibg=#" . a:bg . " ctermbg=" . s:rgb(a:bg)
      endif

      if a:attr != ""
        exec "hi " . a:group . " gui=" . a:attr . " cterm=" . a:attr
      endif
    endfun

  EOSTRING
end

def postlude
  <<~EOSTRING
    " ===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-===-==

    call s:identify_color_mode()
    call s:apply_styles()

    " vim:set et sw=2 fdl=0 fdm=marker:
  EOSTRING
end

INDENT = '  '

# Convert style modifier YAML to boolean map
def modifier_yaml2bools(yaml)
  {
    bold:          (yaml['font-weight']&.   != 'normal'),
    underline:     (yaml['underline']&.     != 'normal'),
    undercurl:     (yaml['undercurl']&.     != 'normal'),
    strikethrough: (yaml['strikethrough']&. != 'normal'),
    standout:      (yaml['standout']&.      != 'normal'),
    italic:        (yaml['italic']&.        != 'normal'),
    reverse:       (yaml['reverse']&.       != 'normal'),
  }
end

# Output appropriate colour values
# TODO FIXME XXX: test
def colour_value_massage(yaml, rgb2index: ->(i) { i })
  %i[fg bg].reduce({}) do |acc, key|
    val = yaml[key.to_s]

    acc.merge(
      key =>
      if val&. != 'transparent'
        rgb2index.call(val)
      else
        'NONE'
      end,
    )
  end
end

def colour2frags(yaml, fg:, bg:, sp: ->(i) {}, rgb2index: ->(i) { i })
  c = colour_value_massage(yaml, rgb2index: rgb2index)
  {
    fg: fg,
    bg: bg,
    sp: sp,
  }.reduce([]) do |acc, (k, cb)|
    if !c[k].nil?
      acc << cb.call(c[k]).to_s
    else
      acc
    end
  end
  # [fg.call(c[:fg]).to_s, bg.call(c[:bg]).to_s, sp.call(c[:sp]).to_s]
end

def mod2frags(yaml, opts, setter: ->(ms) { "cterm=#{ms.join(',')}" })
  mods = modifier_yaml2bools(yaml)

  mod_ary = %i[bold italic reverse underline undercurl standout strikethrough].reduce([]) { |acc, k|
    if mods[k]
      acc << opts[k]
    else
      acc
    end
  }

  mod_ary << opts[:none] if mod_ary.empty?

  [setter.call(mod_ary)]
end

def to_256_styles(styles, bg_darkness, indent_level = 2)
  setter = ->(ms) { "cterm=#{ms.join(',')}" }
  mods   = {
    bold:          'bold',
    none:          'NONE',
    reverse:       'reverse',
    italic:        'italic',
    underline:     'underline',
    undercurl:     'undercurl',
    standout:      'standout',
    strikethrough: 'strikethrough',
  }

  longest_style_length = styles.keys.map(&:length).max
  styles.map { |k, v|
    if !v['link'].nil?
      "#{INDENT * indent_level}hi link #{format("%-#{longest_style_length}s", k)} #{v['link']}"
    elsif !v[bg_darkness]['link'].nil?
      "#{INDENT * indent_level}hi link #{format("%-#{longest_style_length}s", k)} #{v[bg_darkness]['link']}"
    else
      cs        = colour2frags(v[bg_darkness], fg: ->(c) { "ctermfg=#{c}" }, bg: ->(c) { "ctermbg=#{c}" })
      modifiers = cs + mod2frags(v, mods, setter: setter)
      "#{INDENT * indent_level}hi #{format("%-#{longest_style_length}s", k)} #{modifiers.join(' ')}"
    end
  }.join("\n")
end

def to_term_styles(styles, bg_darkness, indent_level = 2)
  setter = ->(ms) { "cterm=#{ms.join(',')}" }
  mods   = {
    none:    'NONE',
    reverse: 'reverse',
  }

  longest_style_length = styles.keys.map(&:length).max
  styles.map { |k, v|
    if !v['link'].nil?
      "#{INDENT * indent_level}hi link #{format("%-#{longest_style_length}s", k)} #{v['link']}"
    elsif !v[bg_darkness]['link'].nil?
      "#{INDENT * indent_level}hi link #{format("%-#{longest_style_length}s", k)} #{v[bg_darkness]['link']}"
    else
      cs        = colour2frags(v[bg_darkness], fg: ->(c) { "ctermfg=#{c}" }, bg: ->(c) { "ctermbg=#{c}" })
      modifiers = cs + mod2frags(v, mods, setter: setter)
      "#{INDENT * indent_level}hi #{format("%-#{longest_style_length}s", k)} #{modifiers.join(' ')}"
    end
  }.join("\n")
end

def to_gui_styles(styles, bg_darkness, indent_level = 2)
  setter = ->(ms) { "cterm=#{ms.join(',')} gui=#{ms.join(',')}" }
  mods   = {
    bold:          'bold',
    none:          'NONE',
    reverse:       'reverse',
    italic:        'italic',
    underline:     'underline',
    undercurl:     'undercurl',
    standout:      'standout',
    strikethrough: 'strikethrough',
  }

  longest_style_length = styles.keys.map(&:length).max
  styles.map { |k, v|
    if !v['link'].nil?
      "#{INDENT * indent_level}hi link #{format("%-#{longest_style_length}s", k)} #{v['link']}"
    elsif !v[bg_darkness]['link'].nil?
      "#{INDENT * indent_level}hi link #{format("%-#{longest_style_length}s", k)} #{v[bg_darkness]['link']}"
    else
      cs        = colour2frags(v[bg_darkness], fg: ->(c) { "guifg=#{c}" }, bg: ->(c) { "guibg=#{c}" }, sp: ->(c) { "guisp=#{c}" })
      modifiers = cs + mod2frags(v, mods, setter: setter)
      "#{INDENT * indent_level}hi #{format("%-#{longest_style_length}s", k)} #{modifiers.join(' ')}"
    end
  }.join("\n")
end

def to_vim(theme_name, styles)
  <<~EOSTRING
    fun! s:apply_styles()
      hi clear
      syntax reset
      let g:colors_name = '#{theme_name}'

      let s:bg_is_dark = &background ==# 'dark'
      if s:bg_is_dark

        if s:mode == s:MODE_GUI_COLOR

    #{to_gui_styles(styles, 'dark', 3)}

        elseif s:mode == s:MODE_256_COLOR

    #{to_256_styles(styles, 'dark', 3)}

        else " Terminal colours

    #{to_term_styles(styles, 'dark', 3)}
        endif

      else

        if s:mode == s:MODE_GUI_COLOR

    #{to_gui_styles(styles, 'light', 3)}

        elseif s:mode == s:MODE_256_COLOR

    #{to_256_styles(styles, 'light', 3)}

        else " Terminal colours

    #{to_term_styles(styles, 'light', 3)}
        endif

      endif
    endfun

  EOSTRING
end

def check_usage
  if ARGV.length != 2
    show_usage
    exit 1
  end
end

def main
  # puts "Got ARGV=#{ARGV}"
  # puts "$PROGRAM_NAME = #{$PROGRAM_NAME}"
  # puts "__FILE__ = #{__FILE__}"
  # puts "$0 = #{$0}"
  check_usage

  # src_file, dest_file = ARGV
  # do_it(src_file, dest_file)
  do_it(*ARGV)
end

main if $PROGRAM_NAME == __FILE__

__END__

