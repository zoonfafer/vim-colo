# See: https://nixos.wiki/wiki/Shell_Scripts#Packaging
#
{ pkgs
, lib
, stdenvNoCC
, bash
, ruby
, makeWrapper
}:
stdenvNoCC.mkDerivation {
  pname = "vim-colo";
  version = "0.0.1";
  src = ./.;
  buildInputs = [ bash ruby ];
  nativeBuildInputs = [ makeWrapper ];
  installPhase = ''
      mkdir -p $out/bin
      cp bin/vim-colo.rb $out/bin/vim-colo
      wrapProgram $out/bin/vim-colo \
        --prefix PATH : ${lib.makeBinPath [ bash ruby ]}
  '';
}
